module.exports = {
  // Отступы — два пробела
  tabWidth: 2,
  // Одинарные кавычки
  singleQuote: true,
  // Точка с запятой в конце строки
  semi: true,
  printWidth: 100,
  trailingComma: 'none',
  bracketSpacing: false
};
